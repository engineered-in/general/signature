# Signature Generator

![PipeLine](https://gitlab.com/engineered-in/general/signature/badges/master/pipeline.svg)
&nbsp; [&#128640;](https://engineered-in.gitlab.io/general/signature/)
&nbsp; [&#8505;](https://gitlab.com/engineered-in/general/signature/-/blob/master/source/index.html)
&nbsp; [&#128240;](https://www.linkedin.com/pulse/deep-link-your-email-signature-swarup-selvaraj/)

## What is Signature Generator?

This is a **Static HTML webpage** which generates deeplinks and 
[URI Schemes](https://en.m.wikipedia.org/wiki/List_of_URI_schemes#Official_IANA-registered_schemes)
to **modernize** your [Email Signature](https://www.linkedin.com/pulse/deep-link-your-email-signature-swarup-selvaraj/).

## What can it Generate?

![Smart Signature](https://engineered-in.gitlab.io/general/signature/res/smart-sign.png)

It can generate light-weight action icons for:

- [x] Website link
- [x] Email **compose action** link
- [x] Phone **call action** link
- [x] Teams **chat deep-link**
- [x] Linkedin profile link
- [x] GitLab profile link
- [x] GitHub profile link
- [x] Location **navigation deep-link**

## Is it Safe?

Totally.  This is a Static HTML webpage (meaning it has **no backend**).

All the Javascripts are processed in your device.

Still unsure? See the [source code](https://gitlab.com/engineered-in/general/signature/-/blob/master/source/index.html#L200-315) yourself.

# Miss Something?

In case if you think something is missing in here, 

- [Check the Issue board](https://gitlab.com/engineered-in/general/signature/-/boards)
- [Create a new Issue](https://gitlab.com/engineered-in/general/signature/-/issues/new) if it is not available in the board.

# Credits

- [HTML 5](https://html.spec.whatwg.org/)
- [Java Script](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/)
- [URI Schemes](https://en.m.wikipedia.org/wiki/List_of_URI_schemes#Official_IANA-registered_schemes)
- [Google Material Design](https://material.io/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) - for hosting Static HTML webpage
- [Wikipedia](https://en.wikipedia.org/wiki/Wikipedia) - for obtaining SVG Product Logo
- [Figma](https://www.figma.com/) - for editing SVG and converting into PNG